"""
Complete the divisibleSumPairs function in the editor below. It should return the integer count of pairs meeting the criteria.
divisibleSumPairs has the following parameter(s):
n: the integer length of array
ar: an array of integers
k: the integer to divide the pair sum by
"""


def divisible_sum_pairs(n, k, ar):
    count = 0
    for i in range(n-1):
        tem_i = ar[i]
        for j in range(1,len(ar[i:])):
            tem_j = ar[j+i]
            if (tem_i + tem_j) % k == 0:
                count += 1
    return count


if __name__ == '__main__':
    n = 6
    k = 3
    ar = [1, 3, 2, 6, 1, 2]
    result = divisible_sum_pairs(n, k, ar)
    print(result)
