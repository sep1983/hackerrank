# Complete the matrixRotation function below.


def matrixRotation(h, r):
    lenH = len(h[0])
    lenY = len(h)

    def serial_list(s):
        return s[0] + [x[-1:][0] for x in s[1:-1]] + s[-1:][0][::-1] + [x[:1][0] for x in s[::-1][1:-1]]

    w1 = serial_list(h)
    if lenY > 2:
        t = [x[1:-1] for x in h[1:-1]]
        lent = len(t[0])
        w2 = serial_list(t)

    for _ in range(r):
        w1 = w1[1:] + [w1[0]]
        if lenY > 2:
            w2 = w2[1:] + [w2[0]]

    if lenY > 2:
        y = [w2[x:x + lent] for x in range(0, len(w2), lent)]
        temp_1 = y[0]
        y[-1].reverse()
        intern = y
        if len(y) > 2:
            intern = [temp_1] + list(map(list,zip(y[-1],y[1:2][0])))

    result = []
    result.append(w1[:lenH])
    print(*w1[:lenH], sep=' ')
    newPlace = lenH

    for i in range(lenY-2):
        if lenY > 2:
            l_temp100 = [w1[-1-i]] + intern[i] + [w1[newPlace:newPlace + 1][0]]
            print(*l_temp100, sep=' ')
            result.append(l_temp100)
            newPlace += 1
        else:
            l_temp200 = [w1[-1-i]] + [w1[newPlace:newPlace + 1][0]]
            print(*l_temp200, sep=' ')
            result.append(l_temp200)

    w1.reverse()
    l_temp300 = w1[lenY-2:(lenY-2 + lenH)]
    print(*l_temp300,sep=' ')
    result.append(l_temp300)
    return result


if __name__ == '__main__':

    matrix = [[1, 2, 3, 4], [7, 8, 9, 10], [13, 14, 15, 16], [19, 20, 21, 22], [25, 26, 27, 28]]
    test = [[28,27,26,25], [22,9,15,19], [16,8,21,13], [10,14,20,7], [4,3,2,1]]
    r = 7

    matrix = [[1, 1], [1, 1]]
    test = [[1,1], [1, 1]]
    r = 3

    matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
    test = [[2, 3, 4, 8], [1, 7, 11, 12], [5, 6, 10, 16], [9, 13, 14, 15]]
    r = 1

    rest = matrixRotation(matrix, r)
    if rest == test:
        print('ok')