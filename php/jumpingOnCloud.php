<?php

/*
 * Complete the 'jumpingOnClouds' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY c as parameter.
 */

function jumpingOnClouds($c) {
  $total = 0;

  for($i = 0; $i < count($c) - 1; $i++){

      if($c[$i] == 1){
          continue;
      }

      if($c[$i + 2] == 0){
          ++$total;
          $i += 1;
      }else{
        ++$total;
      }

      //echo "\$i => $i \$total => $total";
  }

  return $total;

}

/**
$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$n = intval(trim(fgets(STDIN)));

$c_temp = rtrim(fgets(STDIN));

$c = array_map('intval', preg_split('/ /', $c_temp, -1, PREG_SPLIT_NO_EMPTY));

$result = jumpingOnClouds($c);

fwrite($fptr, $result . "\n");

fclose($fptr);
**/

var_dump(4 == jumpingOnClouds([0, 0, 1, 0, 0, 1, 0]));
var_dump(3 == jumpingOnClouds([0, 0, 0, 0, 1, 0]));
