<?php

/*
 * Complete the 'repeatedString' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. LONG_INTEGER n
 */

function repeatedString($s, $n) {

    $cantString = strlen($s);
    $modulo     = $n % $cantString;
    $p          = str_split(subStr($s,0,$modulo));

    $ainStrMod = (in_array('a', $p))? array_count_values($p)['a'] : 0;
    $ainString  = array_count_values(str_split($s))['a'];

    return ((($n - $modulo) / $cantString) * $ainString) + $ainStrMod;

}

/**
$fptr = fopen(getenv("OUTPUT_PATH"), "w");
$s = rtrim(fgets(STDIN), "\r\n");
$n = intval(trim(fgets(STDIN)));
$result = repeatedString($s, $n);
fwrite($fptr, $result . "\n");
fclose($fptr);
**/


var_dump(69801196944  == repeatedString('udjlitpopjhipmwgvggazhuzvcmzhulowmveqyktlakdufzcefrxufssqdslyfuiahtzjjdeaxqeiarcjpponoclynbtraaawrps', 872514961806));
var_dump(1000000000000 == repeatedString('a', 1000000000000));
var_dump(7 == repeatedString('aba', 10));
