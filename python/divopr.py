#!/usr/bin/python3
"""
  Read two integers and print two lines. The first line sho
  uld contain integer division, // . The second line should
  contain float division, /
  You don't need to perform any rounding or formatting
  operations.
"""

def divPrint(a, b = 1):
    return [int(a/b), float(a/b)]

def test_div_0():
    assert divPrint(4, 3) == [1,  1.3333333333333333]

def test_div_1():
    assert divPrint(5, 7) == [0, 0.7142857142857143]

if __name__ == '__main__':
    a = int(input())
    b = int(input())
    res = divPrint(a,b)
    print(res)
    print("{0}\n{1}".format(*res))
