<?php

ini_set('display_error', 1);

function countingValleys($n, $path) {
  $path = str_split($path);
  $total = 0;
  $count = 0;

  foreach ($path as $value) {
     $prev  = $total;
     $total = ($value == 'U')? ++$total : --$total;
     $count = ($total == 0 and $prev < 0)? ++$count: $count;
     //echo('prev=> '. $prev.' tota=>  '.$total.' count=>   '.$count."\n");
  }
  return $count;

}

/**
$fptr = fopen(getenv("OUTPUT_PATH"), "w");
$steps = intval(trim(fgets(STDIN)));
$path = rtrim(fgets(STDIN), "\r\n");
$result = countingValleys($steps, $path);
fwrite($fptr, $result . "\n");
fclose($fptr);
**/

echo var_dump( 1 == countingValleys(8,  'UDDDUDUU'));
echo var_dump( 2 == countingValleys(12, 'DDUUDDUDUUUD'));
echo var_dump( 1 == countingValleys(12, 'UUUDUDUDUUDDDDUDDU'));
echo var_dump( 0 == countingValleys(10, 'UDUUUDUDDD'));
