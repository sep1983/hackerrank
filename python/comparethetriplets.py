#!/usr/bin/python3
#./compatethetriplets

import math
import os
import random
import sys

# Complete the compareTriplets function below.
def compareTriplets(a, b):
    m = sum([1 if x[0] > x[1] else 0 for x in zip(a,b)])
    n = sum([1 if x[1] > x[0] else 0 for x in zip(a,b)])
    return (m,n)

def test_one():
    assert compareTriplets(['17', '28', '30'] , ['99', '16', '8']) == (2, 1), "Assertion failed! one"
    assert compareTriplets(['5', '6', '7'] , ['3', '6', '10']) == (1, 1), "Assertion failed! two"

if __name__ == '__main__':
    #a = list(map(int, input().rstrip().split()))
    #b = list(map(int, input().rstrip().split()))
    result = compareTriplets(a, b)
    print(' '.join(map(str, result)))
