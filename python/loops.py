#!/usr/bin/python3
"""
Loops are control structures that iterate over a range to
qperform a certain task.
There are two kinds of loops in Python.
"""

def loop(n):
    '''
    This function take one number
    It displays the power 2 of n number in the
    range of number that you chose. Return one list
    with the result of all iterations
    '''

    #list comprehension with range
    return [x**2 for x in range(0,n)]


def test_loop_one():
    assert loop(3) == [0,1,4], "Oh no! This assertion failed! 3"


def test_loop_two():
    assert loop(5) == [0,1,4,9,16], "Oh no! This assertion failed! 5"


if __name__ == '__main__':
    # Funtions calling
    n = int(input())
    for v in loop(n):
        print(str(v))
