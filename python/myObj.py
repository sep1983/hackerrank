# !/usr/bin/env/ python
# encoding: utf-8
#
# Copyright (c) 2019 sep1983@tuta.io.
# Some rights reserved

class MyObj(object):
    def __init__(self, num_loops):
        self.count = num_loops

    def go(self):
        for i in range(self.count):
            print(i)
        return

def test_one_go():
   MyObj(3).go == "3\n2\n1" # Execute three times ans show 3 numbers

if __name__ == '__main__':
    MyObj(5).go()
