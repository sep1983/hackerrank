#/usr/bin/python3

from robot import Robot

def test_robot():
    sc = Robot()
    sc.set_name('Gato')
    assert sc.say_hi() == "Hi, I am Gato It's not known, when I was created!"


def test_robot_set():
    sc = Robot()
    assert sc.say_hi() == "Hi, I am a robot without a name It's not known, when I was created!"

def test_robot_get_build_year():
    sc = Robot()
    sc.set_build_year('2020')
    assert sc.get_build_year() == '2020'


