#! /usr/bin/env python

"""
Given an expression string, write a python program to
find whether a given string has balanced parentheses or not
we can use  parenthesis, bracket or brace
"""

class IsBalanced(object):

    def __init__(self, expresion):
        self.tovalidate = expresion
        self.open_list = ["[", "{", "("]
        self.close_list = ["]", "}", ")"]

    def check(self):
        """Function for check if the string
        has balanced parenthesis, bracket or brace
        Each time, when an open parentheses is encountered push it in the
        stack, and when closed parenthesis is encountered, match it with the
        top of stack and pop it. If stack is empty at the end, return Balanced
        otherwise, Unbalanced."""

        stack = []

        if(len(self.tovalidate) == 0):
            return True

        for i in self.tovalidate:
            if i in self.open_list:
                stack.append(i)
            elif i in self.close_list:
                pos = self.close_list.index(i)
                if((len(stack)>0) and (self.open_list[pos] == stack[len(stack)-1])):
                    stack.pop()
                else:
                    return False

        if len(stack) == 0:
            return True

        return False



def test_one():

    assert IsBalanced('').check() == True  #Is is empty

    assert IsBalanced('(a[0]+b[2c[6]]) {24 + 53}').check() == True # It is complete

    assert IsBalanced('f(e(d))').check() == True #Look like function

    assert IsBalanced('[()]{}([])').check() == True #Without elements

    assert IsBalanced('((b)').check() == False #It is incomplete

    assert IsBalanced('(c]').check() == False #It is incomplete parenthesis

    assert IsBalanced('{(a[])').check() == False #Is is incomplete braces

    assert IsBalanced('([)]').check() == False #It is desorganize

    assert IsBalanced(')(').check() == False #It is desorganize but is pair



if __name__ == '__main__':
    print(IsBalanced('((b)').check())
