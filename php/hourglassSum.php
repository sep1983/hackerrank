<?php

/*
 * Complete the 'hourglassSum' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

function hourglassSum($arr) {
    $total = array();
    for($i=0; $i < 4; $i++){
        for($j=0; $j < 4; $j++){
            $total[] =
                $arr[$j][$i]     + $arr[$j][$i + 1]     + $arr[$j][$i + 2]
                + $arr[$j + 1][$i + 1] +
                $arr[$j + 2][$i] + $arr[$j + 2][$i + 1] + $arr[$j + 2][$i + 2];
        }
    }
    return max($total);
}


var_dump(19 == hourglassSum ( array(
    array(1, 1, 1, 0, 0, 0),
    array(0, 1, 0, 0, 0, 0),
    array(1, 1, 1, 0, 0, 0),
    array(0, 0, 2, 4, 4, 0),
    array(0, 0, 0, 2, 0, 0),
    array(0, 0, 1, 2, 4, 0)
)));
