#!/usr/bin/python3

"""
    Read two integers from STDIN and print three lines where:

    The first line contains the sum of the two numbers.
    The second line contains the difference of the two numbers (first - second).
    The third line contains the product of the two numbers.

"""

def towIntegers(a, b):
    addition = a + b
    subtraction = a - b
    division = a * b
    return("{0}\n{1}\n{2}".format(addition, subtraction, division))

def test_Integers_one():
    assert towIntegers(1,3) == "4\n-2\n3"

def test_Integers_two():
    assert towIntegers(3,2) == "5\n1\n6"


if __name__ == '__main__':
    a = int(input())
    b = int(input())
    print(towIntegers(a,b))
