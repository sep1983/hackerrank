"""
    You are given a string .
    Your task is to find the first occurrence of an alphanumeric
    character in (read from left to right) that has consecutive
    repetitions.
"""

import re

def groups_reg(line):
    lineLen = len(line)
    if(lineLen == 0 or lineLen > 100):
        return('-1')
    match = re.search(r'([^\W_])\1+',line)

    try:
        match.groups()
        if match:
            return(match[1])
        else:
            return('-1')
    except:
        return('-1')


def test_one():
    assert groups_reg('..12345678910111213141516171820212223')  == '1'  # it is not leap
    assert groups_reg('')  == '-1'  # it is not leap

    assert groups_reg('HackerRank') == '-1'

    assert groups_reg('..1233456' * 12) == '-1'  # it is greater than 100 not leap

if __name__ == '__main__':
    # Funtions calling
    line = srt(input())
    print(groups_reg(line))
