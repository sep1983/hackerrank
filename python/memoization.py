#!/usr/bin/python3

import time

ef_cache = {}

def exprensive_func(num):
    if num in ef_cache:
        return ef_cache[num]
    print("Computing {}...".format(num))
    time.sleep(1)
    result = num*num
    ef_cache[num] = result
    return result

result = exprensive_func(4)
print(result)

result = exprensive_func(10)
print(result)

result = exprensive_func(4)
print(result)

result = exprensive_func(10)
print(result)
