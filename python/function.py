#!/usr/bin/python3
"""
    We add a Leap Day on February 29, almost every four
    years. The leap day is an extra, or intercalary day
    and we add it to the shortest month of the year, February.
    In the Gregorian calendar three criteria must be
    taken into account to identify leap years:

    The year can be evenly divided by 4, is a leap year, unless:
        The year can be evenly divided by 100, it is NOT a leap year,
    unless:

            The year is also evenly divisible by 400.
    Then it is a leap year.

    This means that in the Gregorian calendar,
    the years 2000 and 2400 are leap
    years, while 1800, 1900, 2100, 2200, 2300 and 2500
    are NOT leap years.
"""

def is_leap(year):
    """
    You are given the year, and the function to check if
    the year is leap or not.
    """
    if((year > 1900) and (year < 10**5)):
        if((year % 4) == 0):
            if(((year % 100) == 0) and ((year % 400) != 0)):
                return False
            return True
        return False
    return False

def test_is_leap_one():
    assert is_leap(1990)  == False  # 1990 it is not leap
    assert is_leap(2000)  == True   # 2000 it is leap
    assert is_leap(2400)  == True   # 2000 it is leap
    assert is_leap(2100)  == False  # 1990 it is not leap
    assert is_leap(1500)  == False  # 1500 this number is not acepted
    assert is_leap(10**6) == False  # 10**6 this number is not acepted


if __name__ == '__main__':
    # Funtions calling
    year = int(input())
    print(is_leap(year))
