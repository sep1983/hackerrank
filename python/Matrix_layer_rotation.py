# Function to rotate a matrix


def rotate_matrix(mat):
    if not len(mat):
        return

    """ 
        top : starting row index 
        bottom : ending row index 
        left : starting column index 
        right : ending column index 
    """

    top = 0
    bottom = len(mat) - 1

    left = len(mat[0]) - 1
    right = 0

    while right < left and bottom > top:
        # Store the first element of next row,
        # this element will replace first element of
        # current row
        prev = mat[top][right]

        # Move elements of topmost column one step down
        for i in range(top + 1, bottom + 1):
            curr = mat[i][right]
            mat[i][right] = prev
            prev = curr

        right += 1

        # Move elements of left row one step right
        for i in range(right, left + 1):
            curr = mat[bottom][i]
            mat[bottom][i] = prev
            prev = curr

        bottom -= 1

        # Move elements of bottom one step top
        for i in range(bottom, top - 1, -1):
            curr = mat[i][left]
            mat[i][left] = prev
            prev = curr

        left -= 1

        # Move elements of rigthmost column one step left
        for i in range(left, right - 2, -1):
            curr = mat[top][i]
            mat[top][i] = prev
            prev = curr

        top += 1



    return mat


# Utility Function
def print_matrix(mat):
    for row in mat:
        print(*row, sep=' ')



if __name__ == '__main__':

    # Test case 1
    matrix = [ [1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16] ]

    lenh = matrix[1,]

    # Test case 2
    """ 
    matrix =[ 
                [1, 2, 3], 
                [4, 5, 6], 
                [7, 8, 9] 
            ] 
    """
    matrix = rotate_matrix(matrix)
    # Print modified matrix
    print_matrix(matrix)
