#!/usr/bin/env python
"""
First Map opening parentheses to respective closing parentheses.
Iterate through the given expression using ‘i’, if ‘i’
is an open parentheses, append in queue, if ‘i’ is close parentheses,
Check whether queue is empty or ‘i’ is the top element of queue,
if yes, return “Unbalanced”, otherwise “Balanced”.
"""

class IsBalanced(object):

    def  __init__(self, expression):
        self.expression = expression
        self.open_tup = tuple('({[')
        self.close_tup = tuple(')}]')

    def check(self):
        sav = dict(zip(self.open_tup, self.close_tup))
        queue = []

        for i in self.expression:
            if i in self.open_tup:
                queue.append(sav[i])
            elif i in self.close_tup:
                if not queue or i != queue.pop():
                    return False

        if queue:
            return False

        return True


def test_two():

    assert IsBalanced('').check() == True  #Is is empty

    assert IsBalanced('(a[0]+b[2c[6]]) {24 + 53}').check() == True # It is complete

    assert IsBalanced('f(e(d))').check() == True #Look like function

    assert IsBalanced('[()]{}([])').check() == True #Without elements

    assert IsBalanced('((b)').check() == False #It is incomplete

    assert IsBalanced('(c]').check() == False #It is incomplete parenthesis

    assert IsBalanced('{(a[])').check() == False #Is is incomplete braces

    assert IsBalanced('([)]').check() == False #It is desorganize

    assert IsBalanced(')(').check() == False #It is desorganize but is pair



if __name__ == '__main__':
    printr
    print(IsBalanced('((b)').check())
