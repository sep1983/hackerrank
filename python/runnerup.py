"""
    Given the participants' score sheet for your University Sports Day,
    you are required to find the runner-up score. You are given scores.
    Store them in a list and find the score of the runner-up.
"""

import re

def runnerup_reg(n, arr):
    #if(n <= 2 or n >= 10):
    zes = max(arr)
    i=0
    while(i<n):
        if zes == max(arr):
            arr.remove(max(arr))
        i+=1
    return(max(arr))

def test_one():
    assert runnerup_reg(5, [2,3,6,6,5]) == 5 # basic example

if __name__ == '__main__':
    # Funtions calling
    n = int(input())
    arr = list(map(int, input().split()))
    print(runnerup_reg(n,arr))
