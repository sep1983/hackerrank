"""
Complete the birthday function in the editor below.
It should return an integer denoting the number
of ways Lily can divide the chocolate bar.

birthday has the following parameter(s):

s: an array of integers, the numbers on each of the squares of chocolate
d: an integer, Ron's birth day
m: an integer, Ron's birth month

"""


def birthday(n, s, d, m):
    if(n==1 and s[0]==d):
        return 1
    elif(n==1):
        return 0
    count = 0
    for i in range(len(s)-1):
        if(sum(s[i:m+i])==d):
            count = count + 1
    return count


if __name__ == '__main__':
    n = 5
    s = [1, 2, 1, 3, 2]
    d = 3
    m = 2

    result = birthday(n, s, d, m)
    print(result)