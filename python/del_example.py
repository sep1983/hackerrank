# /usr/bin/python3

class Robot():
    def __init__(delf, name):
        print(name + " Has been created!")

    def __del__(self):
        print ("Robot has been destroyed")

if __name__ == "__main__":
    x = Robot("Hamtek")
    y = Robot("Collm")
    z = x
    print("Deleting z")
    del z
    del y
    print("Deleting x")
    del x


