# /usr/bin/python3
# https://www.python-course.eu/python3_object_oriented_programming.php

class Robot:

    """ Return the name of the Robot
        >>> r.say_hi()
        "Hi, I am a robot without a name It's not known, when I was created!"

        >>> r.set_name('Gato')
        >>> r.get_name()
        'Gato'

        >>> r.set_build_year('2020')
        >>> r.get_build_year()
        '2020'
    """

    __counter = 0

    def __init__(self,
                 name=None,
                 build_year=None):
        self.__name = name
        self.__build_year = build_year
        self.mess = []
        type(self).__counter += 1

    @staticmethod
    def RobotInstances():
        return Robot.__counter

    @classmethod
    def RobotInstances(cls):
        return cls, Robot.__counter

    def say_hi(self):
        if self.__name:
           self.mess.append('Hi, I am ' + self.__name)
        else:
           self.mess.append('Hi, I am a robot without a name')

        if self.__build_year:
           self.mess.append('I was built in ' + str(self.__build_year))
        else:
            self.mess.append("It's not known, when I was created!")
        return ' '.join(self.mess)

    def __repr__(self):
        return "Robot('" + self.__name + "', " +  str(self.__build_year) +  ")"

    def __str__(self):
        return "Name: " + self.__name + ", Build Year: " +  str(self.__build_year)

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return str(self.__name)

    def set_build_year(self, by):
        self.__build_year = by

    def get_build_year(self):
        return self.__build_year


if __name__ == '__main__':
    import doctest

    print(Robot.RobotInstances())
    x = Robot()
    print(x.RobotInstances())
    y = Robot()
    print(x.RobotInstances())
    print(Robot.RobotInstances())

    doctest.testmod(extraglobs={'r': Robot()})
