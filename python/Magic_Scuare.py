#! /usr/bin/env python
#./magicScuare.py

"""
https://www.hackerrank.com/challenges/magic-square-forming/problem

We define a magic square to be an  matrix of distinct positive integers from
to  where the sum of any row, column, or diagonal of length  is always equal
to the same number: the magic constant.
You will be given a  matrix  of integers in the inclusive range.
We can convert any digit  to any other digit  in the range  at cost of.
Given , convert it into a magic square at minimal cost. Print this cost on a new line.
Note: The resulting magic square must contain distinct integers in the inclusive range .
For example, we start with the following matrix :
"""


def magic_square(square):

    #check which number is present in all the operations
    def valid_best_number():
        v_val = [sum(li) for li in square]
        h_val = [sum(tup) for tup in zip(square)]
        #join list in only one
        t_val = sorted(sorted(v_val) + sorted(h_val))

        #search occurrence
        count={}
        for item in t_val:
            count[item]=t_val.count(item)




def test_one():
    #becomes a magic square at the minimum possible cost.
    assert magic_square([[4,9,2],[3,5,7],[8,5,1]]) == 1
    assert magic_square([[4,8,2],[4,5,7],[6,1,6]]) == 4


if __name__ == '__main__':
    square = []
    for i in range(3):
        square.append(list(map(int,input().rstrip().split())))
    result = magic_square(square)
    print(result)

