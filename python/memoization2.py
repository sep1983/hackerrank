#!/usr/bin/python3
import time


def memoize(f):
    memo = {}
    def helper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return helper


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        res = fib(n-1) + fib(n-2)
        print(n)
        print("the res is: {}".format(str(res)))
        return res


start_time = time.time()
fib = memoize(fib)
print(fib(10))
print("--- %s seconds ---" % (time.time() - start_time))
