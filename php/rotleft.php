<?php

/*
 * Complete the 'rotLeft' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY a
 *  2. INTEGER d
 */

function rotLeft($a, $d) {
    while($a){
        array_push($d, array_shift($d));
        --$a;
    }
    return $d;
}

var_dump([5, 1, 2, 3, 4] == rotLeft(4, [1, 2, 3, 4, 5]));
