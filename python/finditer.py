"""
    You are given a string .
    Your task is to find the first occurrence of an alphanumeric
    character in (read from left to right) that has consecutive
    repetitions.
"""

import re

def founditer_reg(line):
    lineLen = len(line)
    if(lineLen == 0 or lineLen > 100):
        return(['-1'])

    vowels = '[aeiou]'
    consonants = '[bcdfghjklmnpqrstvwxyz]'
    regex = '(?<=' + consonants +')(' + vowels + '{2,})' + consonants
    try:
        res = re.findall(regex, line, re.I)
        if res:
            return(res)
        else:
            return(['-1'])
    except:
        return(['-1'])

def test_one():
    assert founditer_reg('rabcdeefgyYhFjkIoomnpOeorteeeeet')  == ['ee', 'Ioo', 'Oeo', 'eeeee'] # Groups of element
    assert founditer_reg('')  == ['-1'] # emtpy
    assert founditer_reg('rabcdefgyhFjkomnporte')  == ['-1'] # emtpy

if __name__ == '__main__':
    # Funtions calling
    line = input()
    print(founditer_reg(line))
    print('\n'.join(founditer_reg(line) or ['-1']))
