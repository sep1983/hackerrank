"""
Complete the migratoryBirds function in the editor below.
It should return the lowest type number of the most frequently sighted bird.
migratoryBirds has the following parameter(s):

arr: an array of integers representing types of birds sighted
"""


def migratory_birds(arr):
    arr.sort()
    reverse_list = arr[::-1]
    option = dict()
    best = 0, 0
    for i in reverse_list:
        if i in option:
            option[i] += 1
        else:
            option[i] = 1

        if option[i] >= best[1]:
            best = i, option[i]

    return best[0]

"""
def migratory_birds(arr):
    return max(set(arr), key=arr.count )
"""

if __name__ == '__main__':
    arr_count = 11

    arr = [1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4]

    result = migratory_birds(arr)
    print(result)
