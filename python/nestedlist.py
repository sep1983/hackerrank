"""
    Given the names and grades for each student in a Physics class of
    students, store them in a nested list and print the name(s) of any
    student(s) having the second lowest grade.
    Note: If there are multiple students with the same grade, order their names
    alphabetically and print each name on a new line.
"""

import re

def studen_score(arr):
    arr = sorted(arr, key=lambda x: x[0], reverse=False)
    arr = sorted(arr, key=lambda x: x[1], reverse=False)

    lestrep = arr.pop(0)

    arr = [ x for x in  arr if x[1] != lestrep[1] ]

    valirep = arr[0][1]
    listrep = []

    for x in range(len(arr)):
        if arr[x][1] == valirep:
            listrep.append(arr[x][0])

    return(listrep)


def test_one():
    listtest = [["Harry", 37.21], ["Berry", 37.21], ["Tina", 37.2], ["Akriti", 41], ["Harsh", 39]]
    assert studen_score(listtest) == ["Berry", "Harry"] # basic example

    listtest = [["H", 21.1], ["B", 21.1], ["Tina", 7.2], ["Akriti", 4], ["Harsh", 3]]
    assert studen_score(listtest) == ["Akriti"] # basic example

    listtest = [["H", 21.1], ["B", 21.1], ["Tina", 7.2], ["Akriti", 3], ["Harsh", 3]]
    assert studen_score(listtest) == ["Tina"] # basic example

    listtest = [["Akriti", 3], ["Harsh", 1]]
    assert studen_score(listtest) == ["Akriti"] # basic example

    listtest = [["Akriti", 2], ["Harsh", 3]]
    assert studen_score(listtest) == ["Harsh"] # basic example

    listtest = [["Akriti", 3], ["Harsh", 3], ['Solin', 1]]
    assert studen_score(listtest) == ["Akriti", "Harsh"] # basic example

    listtest = [["Harsh", 20], ["Beria", 20], ["Varun", 19], ["Kakunami", 19], ["Vikas", 21]]
    assert studen_score(listtest) == ["Beria", "Harsh"] # basic example

    listtest =[["Rachel", -50], ["Mawer", -50], ["Sheen", -50], ["Shaheen", 51]]
    assert studen_score(listtest) == ["Shaheen"] # basic example

if __name__ == '__main__':
    # Funtions calling
    allstudent = list()
    for _ in range(int(input())):
        name = input()
        score = float(input())
        allstudent.append([name,score])

    print('\n'.join(studen_score(allstudent)))
